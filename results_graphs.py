from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import seaborn.objects as so

# choose technology selections
tech_selections = {'Space heating': ['electric_heater', 'gshp', 'store_heat'],
                   'Transport': ['car_ev', 'car_ice']}
                   # 'Roof techs': ['pv', 'st_fp', 'st_vt'],
                   # 'Storage techs': ['store_electricity', 'store_heat']}
# list of technologies for which capacities should be displayed
tech_list = [tech for tech_sel in tech_selections.values() for tech in tech_sel]

# establish file path of root directory of the repository:
reporoot_dir = Path(__file__).resolve().parent

# read capacity file into pandas
capacity_file = reporoot_dir / 'Results' / 'results_energy_cap.csv'
cap_df = pd.read_csv(capacity_file, sep=',', header=0)

# rad production file into pandas
production_file = reporoot_dir / 'Results' / 'results_carrier_prod.csv'
prod_df = pd.read_csv(production_file, sep=',', header=0)

def capacities_bar(tech_sel, tech_cap_df, title):
    tech_cap = tech_cap_df.loc[(cap_df['techs'].isin(tech_sel)) & (cap_df['locs'] != 'EG')]
    p = so.Plot(tech_cap, x="techs", y="energy_cap", color='locs').add(so.Bar(), so.Stack())
    p.show()

def capacities_scatter(tech_sel, tech_cap_df: pd.DataFrame, title):
    sns.catplot(data=tech_cap_df, x='techs', y='energy_cap')
    plt.title(title)
    plt.savefig(reporoot_dir / 'Graphs' / (title + '_scatter.png'), format='png')
    plt.close()

def production_area(tech_sel, prod_df, title):
    # list of columns to group for stacked area graph of production of all households
    col_group_list = ['timesteps', 'techs', 'carriers']
    tech_prod = prod_df.loc[(prod_df['techs'].isin(tech_sel)) & (prod_df['locs'] != 'EG')]
    tech_prod_sum = tech_prod.groupby(col_group_list).sum().reset_index().drop('carriers', axis=1)
    tick_labels = [timestep for timestep in tech_prod_sum.iloc[::24, :]['timesteps']]
    p = so.Plot(tech_prod_sum, 'timesteps', 'carrier_prod', color='techs').add(so.Area(alpha=0.7), so.Stack())
    p.scale(x=so.Continuous().tick(at=tick_labels))
    p.show()

for title, tech_sel in tech_selections.items():
    tech_cap_df = cap_df.loc[(cap_df['techs'].isin(tech_sel)) & (cap_df['locs'] != 'EG')]
    capacities_bar(tech_sel, cap_df, title)
    # capacities_scatter(tech_sel, cap_df, title)
    # production_area(tech_sel, prod_df, title)

