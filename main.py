
import calliope
import create_input 
import run 

scenarios_string = 'carbon_budget_100,res_2h'
path_to_model_yaml = 'model.yaml'
calliope.set_log_verbosity("info", include_solver_output=True, capture_warnings=True)
model = calliope.Model(path_to_model_yaml, scenario=scenarios_string)

model = run.update_constraint_sets(model)
model.run(build_only=True)

run.add_eurocalliope_constraints(model)
new_model = model.backend.rerun()


#new_model.plot.summary(to_file="barbapapà.html")
#model.plot.timeseries(subset={'costs': ['monetary']})
#model.plot.summary(to_file='summary_results_SD.html')
#model.plot.transmission()
#model.to_csv("Results_SD")
caps=new_model.get_formatted_array("energy_cap").to_pandas()
